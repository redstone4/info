# Info

Hello, this is the red-stone project for writing a server for minecraft.  I don't have anything setup for donations or anything, this project depends on people willing to put work in towards having a minecraft-server software that doesn't use Log4J.  The purpose behind this project is specifically because of the security-vulnerabilities of Log4J.  

This project isn't going to be shared on social media until it is ready.  

##   Contact info

Discord-profile:   `circuitcoder_`

_Please note that I may not be on discord all the time, but I do login and check my inbox, so be patient.  Additionally, this is such a new project that it won't have any money, because there's no dontations.  Donations may be in bitcoin later, when that is setup.  I'm currently the only one working on this, but help would be apprechiated.  Work done on this project by a developer will have a object-path and name of that object with the methods listed under the name of the author, so that I know who did what, and that information will be gathered from diffs._  
